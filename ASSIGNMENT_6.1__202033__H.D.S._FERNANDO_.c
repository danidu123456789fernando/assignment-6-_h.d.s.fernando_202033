#include <stdio.h>

void pattern(int a);

void Row(int b); 

int NumberOfRows=1;
 
int r; 

void pattern(int a)
{  
    
    if(a>0)
    {
        Row(NumberOfRows);
        printf("\n");
        NumberOfRows++;
        pattern(a-1);
        
    }
}

void Row(int b)
{
    if(b>0)
    {
        printf("%d",b);
        Row(b-1);
    }
}

int main()
{
    
    printf("ENTER HOW MANY LINES YOU WANT TO PRINT ? Pattern");
    scanf("%d",&r);
    pattern(r);
    return 0;
}
